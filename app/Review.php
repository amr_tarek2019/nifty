<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table='reviews';
    protected $fillable=['station_id', 'user_id', 'rating', 'comment'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

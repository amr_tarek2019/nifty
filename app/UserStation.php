<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStation extends Model
{
    protected $table='users_stations';
    protected $fillable=['user_id', 'station_id', 'date', 'Fuel_Amount_Paid', 'Donation_Amount','order_number'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function station()
    {
        return $this->belongsTo('App\Station','station_id');
    }
}

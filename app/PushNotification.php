<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class PushNotification extends Model
{

        public static function send($tokens, $msg , $type)
        {

            $fields = array
                (
                "registration_ids" => $tokens,
                "priority" => 10,
                'data' => [
                  'title' => "",
                  'sound' => 'default',
                  'message' => $msg,
                  'type' => $type
                ],
                'notification' => [
                    'type'    => $type,
                    'text' => $msg,
                    'title' => "",
                    'sound' => 'default'
                ],
                'vibrate' => 1,
                'sound' => 1
            );
            $headers = array
                (
                'accept: application/json',
                'Content-Type: application/json',
                'Authorization: key=' .
                // 'AIzaSyDMeSqZgUzQaPeVEm-KsTusRUVGFiRbW80'
                // 'AAAA1kqMc_o:APA91bHgPRiYdC5e7yDmXEWaxSnmfOQPUXOJGRtIitRiIhlwXsnCzVyOmayal1tRHIygIZKGAoWsyYUNl7PJAAF0qfSRQVsrDJ8GXRRxWpQxKSttXkRGPnXMRyow99ncfUFbVIewF7aBXnJszQsIL5XHgUtSmBw1Aw'
                //'APA91bE_dEDuiJp-nYOZP6urgp7lF7b-83z2Kpk5jqSZAVe0wcodV_9HOjAsUibH-obqBf6siF5ZUnIJFNWb84D4rX-XtyCVEP4JTubgQc5D76HgBhc3wkovAhrd6nMzq3kuF5jSVhtn'
'AAAAt_tQg_o:APA91bFzv1r5BNn-XHzyE4hsbbg010kiJCIcMwyBnC3TBO31Hsi7RgeXx2ejo7q6eqDDVLhKSBUVd4Eb8LWFSBtdr3jLhGo3BnchN1oqRM3T35NGLFvdf_6UlU76a5jIkCdeT3MmgEf8'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            //  var_dump($result);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);
            return $result;
        }


        public static function send_details($tokens,$title,$message, $details ,$type='')
        {

            $fields = array
                (
                "registration_ids" => $tokens,
                "priority" => 10,
                'data' => [
                  'title' => $title,
                  'details' => $message,
                  //'details' => $details,
                  'type' => $type,
                ],
                'notification' => [
//                  'title' => $title,
                  'message' => $message,
                  'body' => $message,
                  'details' => $details,
                  'type' => $type,
                  'title' => "",
                  'sound' => 'default'
                ],
                'vibrate' => 1,
                'sound' => 1
            );
            $headers = array
                (
                'accept: application/json',
                'Content-Type: application/json',
                'Authorization: key=' .
                // 'AIzaSyDMeSqZgUzQaPeVEm-KsTusRUVGFiRbW80'
                // 'AAAA1kqMc_o:APA91bHgPRiYdC5e7yDmXEWaxSnmfOQPUXOJGRtIitRiIhlwXsnCzVyOmayal1tRHIygIZKGAoWsyYUNl7PJAAF0qfSRQVsrDJ8GXRRxWpQxKSttXkRGPnXMRyow99ncfUFbVIewF7aBXnJszQsIL5XHgUtSmBw1Aw'
               // 'AAAAXHIOOno:APA91bEUH19-Cw9e80uj_GLYfOHPMfF7Zn4AfBViphboa3Jp8xdsxUgyvDn8ChElA2qQl_S2R-VRw1-InuKSKR9M11sxZ2COuvhBsrNFWO5SEpFF_PiNfgJF5L6t6mT2JKmSjKpx4PlV'
              'AAAAt_tQg_o:APA91bFzv1r5BNn-XHzyE4hsbbg010kiJCIcMwyBnC3TBO31Hsi7RgeXx2ejo7q6eqDDVLhKSBUVd4Eb8LWFSBtdr3jLhGo3BnchN1oqRM3T35NGLFvdf_6UlU76a5jIkCdeT3MmgEf8'

            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            //  var_dump($result);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);
            return $result;
        }
        public static function send_details_driver($tokens,$title,$message, $details , $q=null,$type='')
        {

            $fields = array
                (
                "registration_ids" => $tokens,
                "priority" => 10,
                'data' => [
                  // 'title' => $msg,
//                  'title' => $title,
                  'message' => $message,
                  'details' => $details,
                  'type' => $type,
                  'title' => ""
                ],
                'notification' => [
//                    'title' => $title,
                    'message' => $message,
                    'body' => $message,
                    'details' => $details,
                    'type' => $type,
                    'title' => ""
                ],
                'vibrate' => 1,
                'sound' => 1
            );
            $headers = array
                (
                'accept: application/json',
                'Content-Type: application/json',
                'Authorization: key=' .
                // 'AIzaSyDMeSqZgUzQaPeVEm-KsTusRUVGFiRbW80'
                // 'AAAA1kqMc_o:APA91bHgPRiYdC5e7yDmXEWaxSnmfOQPUXOJGRtIitRiIhlwXsnCzVyOmayal1tRHIygIZKGAoWsyYUNl7PJAAF0qfSRQVsrDJ8GXRRxWpQxKSttXkRGPnXMRyow99ncfUFbVIewF7aBXnJszQsIL5XHgUtSmBw1Aw'
               'AAAAt_tQg_o:APA91bFzv1r5BNn-XHzyE4hsbbg010kiJCIcMwyBnC3TBO31Hsi7RgeXx2ejo7q6eqDDVLhKSBUVd4Eb8LWFSBtdr3jLhGo3BnchN1oqRM3T35NGLFvdf_6UlU76a5jIkCdeT3MmgEf8'

            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            //  var_dump($result);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);
            return $result;
        }
}

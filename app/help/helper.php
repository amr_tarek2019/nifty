<?php
function distance($lat1, $lon1, $lat2, $lon2, $unit = "K") {
if (($lat1 == $lat2) && ($lon1 == $lon2)) {
return 0;
}
else {
$theta = $lon1 - $lon2;
$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
$dist = acos($dist);
$dist = rad2deg($dist);
$miles = $dist * 60 * 1.1515;
$unit = strtoupper($unit);
if ($unit == "K") {
return ($miles * 1.609344);
} else if ($unit == "N") {
return ($miles * 0.8684);
} else {
return $miles;
}
}
}
function unreadMsg()
{
    return \App\Suggestion::where('view',0)->get();
}

function countUnreadMsg()
{
    return \App\Suggestion::where('view',0)->count();
}


function unActiveUser()
{
    return \App\User::where('user_status',0)->where('user_type','user')->get();
}

function countUnActiveUser()
{
    return \App\User::where('user_status',0)->where('user_type','user')->count();
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charity extends Model
{
    protected $table='charity';
    protected $fillable=['name_e','name_a','image'];

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/charity/'.$value);
        } else {
            return asset('uploads/user/profile/default.png');
        }
    }
    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/charity/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }
}

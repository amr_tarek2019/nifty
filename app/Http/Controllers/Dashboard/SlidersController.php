<?php

namespace App\Http\Controllers\Dashboard;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::all();
        return view('dashboard.views.slider.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.views.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title_a' => 'required',
            'title_e' => 'required',
            'text_e' => 'required',
            'text_a' => 'required',
            'image' => 'required|mimes:jpeg,jpg,bmp,png',
        ]);
        $slider = new Slider();
        $slider->title_a = $request->title_a;
        $slider->title_e = $request->title_e;
        $slider->text_e = $request->text_e;
        $slider->text_a = $request->text_a;
        $slider->image =  $request->image;
        if ($slider->save())
        {
            return redirect()->route('slider.index')->with('successMsg','Slider Created Successfully');
        }
        return redirect()->route('slider.create')->with('successMsg','sorry something went wrong');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('dashboard.views.slider.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $slider=Slider::find($request->id);
        $slider->title_a = $request->title_a;
        $slider->title_e = $request->title_e;
        $slider->text_e = $request->text_e;
        $slider->text_a = $request->text_a;
        $slider->image =  $request->image;
        if ($slider->save())
        {
            return redirect()->route('slider.index')->with('successMsg','Slider Updated Successfully');
        }
        return redirect()->route('slider.create')->with('successMsg','sorry something went wrong');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::find($id);
        $slider->delete();
        return redirect()->back()->with('successMsg','Slider Successfully Delete');
    }
}

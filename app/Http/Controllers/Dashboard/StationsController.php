<?php

namespace App\Http\Controllers\Dashboard;

use App\Station;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stations=Station::all();
        return view('dashboard.views.station.index',compact('stations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.views.station.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name_e' => 'required',
            'name_a' => 'required',
            'latitude'=>'required',
            'longitude'=>'required',
            'region_e' => 'required',
            'region_a' => 'required',
            'rate' => 'required',
            'image' => 'required|mimes:jpeg,jpg,bmp,png',
            'qr'=> 'required|mimes:jpeg,jpg,bmp,png',
            'description_e'=>'required',
            'description_a'=>'required',
            'icon'=> 'required|mimes:jpeg,jpg,bmp,png',
            'qr_code'=>'required',
            'image1' => 'required|mimes:jpeg,jpg,bmp,png',
            'image2' => 'required|mimes:jpeg,jpg,bmp,png',
        ]);
        $station = new Station();
        $station->name_e = $request->name_e;
        $station->name_a = $request->name_a;
        $station->latitude = $request->latitude;
        $station->longitude = $request->longitude;
        $station->region_e = $request->region_e;
        $station->region_a = $request->region_a;
        $station->rate=$request->rate;
        $station->image =  $request->image;
        $station->qr =  $request->qr;
        $station->description_e = $request->description_e;
        $station->description_a = $request->description_a;
        $station->icon =  $request->icon;
        $station->qr_code =  $request->qr_code;
        $station->image1 =  $request->image1;
        $station->image2 =  $request->image2;
          //dd($station);
        if ($station->save())
        {
            return redirect()->route('station.index')->with('successMsg','Station Created Successfully');
        }
        return redirect()->route('station.create')->with('successMsg','sorry something went wrong');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $station = Station::find($id);
        return view('dashboard.views.station.edit',compact('station'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $station = Station::find($id);
        $station->name_e = $request->name_e;
        $station->name_a = $request->name_a;
        $station->latitude = $request->latitude;
        $station->longitude = $request->longitude;
        $station->region_e = $request->region_e;
        $station->region_a = $request->region_a;
        $station->rate=$request->rate;
        $station->image =  $request->image;
        $station->qr =  $request->qr;
        $station->description_e = $request->description_e;
        $station->description_a = $request->description_a;
        $station->icon =  $request->icon;
        $station->qr_code =  $request->qr_code;
        $station->image1 =  $request->image1;
        $station->image2 =  $request->image2;

        if ($station->save())
        {
            return redirect()->route('station.index')->with('successMsg','Station Updated Successfully');
        }
        return redirect()->route('station.create')->with('successMsg','sorry something went wrong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $station = Station::find($id);
        $station->delete();
        return redirect()->route('station.index')->with('successMsg','Station Deleted Successfully');
    }
}

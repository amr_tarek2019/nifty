<?php

namespace App\Http\Controllers\Api\Authentication;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthenticationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'password' => 'required',
            'confirm_password'=>'required',
            'payment_method' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $verify_code = rand(1111, 9999);
        $jwt_token = Str::random(25);
        $user=User::where('phone',$request->phone)->orWhere('email',$request->email)->exists();
        if ($user){
            return $response=[
                'status'=>'failed',
                'message'=>'this acount submitted before',
            ];
        }
        $user = User::create(array_merge($request->all(),[
            'jwt_token' => $jwt_token,
            'verify_code' => $verify_code,
            'balance'=>'0',
            'firebase_token'=>'0',
            'user_type'=>'user'
        ]));
//        return $this->sendResponse($user, 'User register successfully.');
        $response=[
            'status'=>202,
            'data'=>$user,
            'message'=>'User register successfully.',
        ];
        return \Response::json($response,202);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if (auth()->attempt(['email' => $request->input('email'),
                    'password' => $request->input('password')])) {
                    $user = auth()->user();
        //            $data['id'] = $user->id;
        //            $data['jwt_token'] = $user->jwt_token;
                    if($user) {
                        return response()->json([
                            'status'=>202,
                            'message'=>'تم تسجيل الدخول',
                            'data'=>$user
                        ]);
                    }
                } else {

                    return response()->json([
                        'status'=>401,
                        'message'=>'تاكد من صحه البيانات'
                    ]);
                }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function forgetPassword(Request $request)
    {
        $this->validate($request,[
            'phone' => 'required'
        ]);
        $user = User::where('phone', request('phone'))->first();
        $verify_code = rand(1111, 9999);

        if (!empty($user)) {
            $user->verify_code=$verify_code;
            $user->save();
            $response=[
                'message'=>'code sent successfully',
                'status'=>202,
                'data' => ['code'=>$user->verify_code],
            ];
            return \Response::json($response,202);

        }else{
            // return \Response::json('phone not found',404);
            $response=[
                'message'=>'phone not found',
                'status'=>404,
            ];
            return \Response::json($response,404);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\  $app
     * @return \Illuminate\Http\Response
     */
    public function verifyCode(Request $request)
    {
        $this->validate($request,[
            'verify_code' => 'required',
        ]);
        $user = User::where('verify_code', request('verify_code'))->first();
        if (!empty($user)) {
            $user->verify_code='0';
            $user->save();
            $response=[
                'message'=>'your account verified',
                'status'=>202,
                'data'=>$user,
            ];

            return \Response::json($response,202);

        }else{
            // return \Response::json('code activation not found',404);
            $response=[
                'message'=>'code activation not found',
                'status'=>404,
            ];

            return \Response::json($response,200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\  $app
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(Request $request)
    {
//        $user=User::find($request->id);
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token', $jwt)->first();
        $this->validate($request,[
            'password' => 'required',
            'password_confirmation'=>'required',
        ]);
        if ($request->password !=null&&($request->password==$request->password_confirmation)){
            $user->password=$request->password_confirmation;
        }
        if ($user->save())
        {
            $response=[
                'message'=>'new password changed successfully',
                'status'=>202,
                'data'=>$user,
            ];

            return \Response::json($response,202);

        }else{
            $response=[
                'message'=>'something went wrong',
                'status'=>401,
                'data'=>$user,
            ];

            return \Response::json($response,401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\  $app
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, App $app)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\  $app
     * @return \Illuminate\Http\Response
     */
    public function destroy(App $app)
    {
        //
    }
}

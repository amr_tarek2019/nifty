<?php

namespace App\Http\Controllers\Api\Stations;

use App\Category;
use App\City;
use App\CompanySeller;
use App\Privacy;
use App\Reservation;
use App\StationImages;
use App\TakeDownContainer;
use App\User;
use App\WasteContainer;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Station;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class StationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'e';
        $station_list=Station::select('id','name_'.$lang. ' as name'
        ,'latitude','longitude','region_'.$lang. ' as region','rate','image','qr','description_'.$lang. ' as description',
            'icon','image1','image2')->get();
       // $data['station_images'] = StationImages::where('id',$station_list)->select('id','image1','image2')->first();

        $response=[
            'message'=>'get data of stations successfully',
            'status'=>202,
            'data'=>$station_list,
        ];

        return \Response::json($response,202);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexRate(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'e';
        $station_list=Station::select('id','name_'.$lang. ' as name'
            ,'latitude','longitude','region_'.$lang. ' as region','rate','image','qr','description_'.$lang. ' as description',
            'icon','image1','image2')->orderBy('rate', 'desc')->take(2)->get();
        $response=[
            'message'=>'get data of stations successfully',
            'status'=>202,
            'data'=>$station_list,
        ];

        return \Response::json($response,202);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function indexLocation(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'e';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $user->longitude = $request->longitude;
        $user->latitude=$request->latitude;
        $station_list=DB::table("stations")
            ->select("id",'name_'.$lang. ' as name'
                ,'latitude','longitude','region_'.$lang. ' as region','rate','image','qr','description_'.$lang. ' as description',
                'icon','image1','image2'
                ,DB::raw("3959 * acos(cos(radians(" . $user->latitude . ")) 
        * cos(radians(latitude)) 
        * cos(radians(longitude) - radians(" .  $user->longitude . ")) 
        + sin(radians(" .$user->latitude. ")) 
        * sin(radians(latitude))) AS distance"))->orderBy('distance', 'asc')->take(2)->get();
        $response=[
            'message'=>'get data of stations successfully',
            'status'=>202,
            'data'=>$station_list,
        ];

        return \Response::json($response,202);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function SearchByRate(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'e';
        $validator = Validator::make($request->all(), [
            'rate' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $rate = $request->rate;
        $stations = Station::select('id','name_'.$lang. ' as name'
            ,'latitude','longitude','region_'.$lang. ' as region','rate','image','qr','description_'.$lang. ' as description',
            'icon','image1','image2')->where([
            ['rate', 'LIKE', '%' . $rate . '%']])->get();
        $response=[
            'message'=>'get data of stations successfully',
            'status'=>202,
            'data'=>$stations,
        ];
        return \Response::json($response,202);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function SearchByLocation(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'e';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $user->longitude = $request->longitude;
        $user->latitude=$request->latitude;
        $station_list=DB::table("stations")
            ->select("id",'name_'.$lang. ' as name'
                ,'latitude','longitude','region_'.$lang. ' as region','rate','image','qr','description_'.$lang. ' as description',
                'icon','image1','image2'
                ,DB::raw("3959 * acos(cos(radians(" . $user->latitude . ")) 
        * cos(radians(latitude)) 
        * cos(radians(longitude) - radians(" .  $user->longitude . ")) 
        + sin(radians(" .$user->latitude. ")) 
        * sin(radians(latitude))) AS distance"))->orderBy('distance', 'asc')->get();
        $response=[
            'message'=>'get data of stations successfully',
            'status'=>202,
            'data'=>$station_list,
        ];

        return \Response::json($response,202);
    }

//    public function SearchByRegionE(Request $request)
//    {
//        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'e';
//        $validator = Validator::make($request->all(), [
//            'region_e' => 'required',
//        ]);
//        if ($validator->fails()) {
//            return $this->sendError('Validation Error.', $validator->errors());
//        }
//        $region_e = $request->region_e;
//        $stations = Station::select('id','name_'.$lang. ' as name'
//            ,'latitude','longitude','region_'.$lang. ' as region','rate','image','qr','description_'.$lang. ' as description',
//            'icon','image1','image2')->where([
//            ['region_e', 'LIKE', '%' . $region_e . '%']])->get();
//        $response=[
//            'message'=>'get data of stations successfully',
//            'status'=>202,
//            'data'=>$stations,
//        ];
//        return \Response::json($response,202);
//    }
//
//    public function SearchByRegionA(Request $request)
//    {
//        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'e';
//        $validator = Validator::make($request->all(), [
//            'region_a' => 'required',
//        ]);
//        if ($validator->fails()) {
//            return $this->sendError('Validation Error.', $validator->errors());
//        }
//        $region_a = $request->region_a;
//        $stations = Station::select('id','name_'.$lang. ' as name'
//            ,'latitude','longitude','region_'.$lang. ' as region','rate','image','qr','description_'.$lang. ' as description',
//            'icon','image1','image2')->where([
//            ['region_a', 'LIKE', '%' . $region_a . '%']])->get();
//        $response=[
//            'message'=>'get data of stations successfully',
//            'status'=>202,
//            'data'=>$stations,
//        ];
//        return \Response::json($response,202);
//    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

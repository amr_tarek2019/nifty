<?php

namespace App\Http\Controllers\Api\FuelPaymentDonation;

use App\CompanySeller;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\OrderDetail;
use App\Reservation;
use App\Station;
use App\Suggestion;
use App\TakeDownContainer;
use App\User;
use App\UserStation;
use App\WasteContainer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FuelPaymentDonationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'e';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();

        $validator = Validator::make($request->all(), [
            'Fuel_Amount_Paid' => 'required',
            'Donation_Amount' => 'required',
            'date'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $stationId = Station::where('id',$request->station_id)->first();
        $order_number = rand(111111111, 999999999);
        $pay=new UserStation();
        $pay->user_id=$user->id;
        $pay->order_number=$order_number;
        $pay->station_id=$stationId->id;
        $pay->Fuel_Amount_Paid = $request->Fuel_Amount_Paid;
        $pay->Donation_Amount = $request->Donation_Amount;
        $pay->date=$request->date;

        $pay->save();


        $response=[
            'message'=>'Payment request sent successfully',
            'status'=>'202',
        ];
        return \Response::json($response,202);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'e';
        $payment = UserStation::where('id',$request->id)->get();
        $payment_item = [];
        $payment_list  = [];
        foreach ($payment as $pay) {
            $payment_item['id'] = $pay->id;
            $payment_item['order_number'] = $pay->order_number;
            $payment_item['station_details'] = Station::where('id',$pay->station_id)->select('image','name_'.$lang.' as name')->first();
            $payment_item['date'] = $pay->date;
            $payment_item['Fuel_Amount_Paid'] = $pay->Fuel_Amount_Paid;
            $payment_item['Donation_Amount'] = $pay->Donation_Amount;
            $payment_list[] = $payment_item;

        }

        $response = [
            'message' => 'get data of User Payment successfully',
            'status' => 202,
            'data' => $payment_list,
        ];
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function indexHistory(Request $request)
    {

        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'e';

        $user = User::where('jwt_token', $jwt)->first();
        $histories = UserStation::where('user_id', $user->id)->get();

        $history_item = [];
        $history_list  = [];
        foreach ($histories as $history) {
            $history_item['id'] = $history->id;
            $history_item['order_number'] = $history->order_number;
            $history_item['station'] = Station::where('id',$history->station_id)->select('name_'.$lang.' as name')->first();
            $history_item['date'] = $history->date;
            $history_item['total'] = $history->Fuel_Amount_Paid+$history->Donation_Amount	;
            $history_list[] = $history_item;

        }

        $response = [
            'message' => 'get data of User Reservations successfully',
            'status' => 202,
            'data' => $history_list,
        ];
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }


    public function indexTransactions(Request $request)
    {

        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'e';

        $user = User::where('jwt_token', $jwt)->first();
        $transactions = UserStation::where('user_id', $user->id)->get();

        $transaction_item = [];
        $transaction_list  = [];
        foreach ($transactions as $transaction) {
            $transaction_item['id'] = $transaction->id;
            $transaction_item['station'] = Station::where('id',$transaction->station_id)->select('name_'.$lang.' as name','icon')->first();
            $transaction_item['Fuel_Amount_Paid'] = $transaction->Fuel_Amount_Paid;
            $transaction_list[] = $transaction_item;

        }

        $response = [
            'message' => 'get data of User Reservations successfully',
            'status' => 202,
            'data' => $transaction_list,
        ];
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }
}

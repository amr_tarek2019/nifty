<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    protected $table='stations';
    protected $fillable=['name_e', 'name_a', 'latitude', 
        'longitude', 'region_e', 'region_a', 'rate', 'image', 'qr', 'description_e', 'description_a',
        'qr_code','image1','image2'];

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/station/image/'.$value);
        } else {
            return asset('uploads/station/image/default.png');
        }
    }
    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/station/image/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }
    public function getImage1Attribute($value)
    {
        if ($value) {
            return asset('uploads/station/images/'.$value);
        } else {
            return asset('uploads/station/images/default.png');
        }
    }
    public function setImage1Attribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/station/images/'),$imageName);
            $this->attributes['image1']=$imageName;
        }
    }
    public function getImage2Attribute($value)
    {
        if ($value) {
            return asset('uploads/station/images/'.$value);
        } else {
            return asset('uploads/station/images/default.png');
        }
    }
    public function setImage2Attribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/station/images/'),$imageName);
            $this->attributes['image2']=$imageName;
        }
    }
    public function getQrAttribute($value)
    {
        if ($value) {
            return asset('uploads/station/qr_code/'.$value);
        } else {
            return asset('uploads/station/qr_code/default.png');
        }
    }
    public function setQrAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/station/qr_code/'),$imageName);
            $this->attributes['qr']=$imageName;
        }
    }

    public function getIconAttribute($value)
    {
        if ($value) {
            return asset('uploads/station/icon/'.$value);
        } else {
            return asset('uploads/station/icon/default.png');
        }
    }
    public function setIconAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/station/icon/'),$imageName);
            $this->attributes['icon']=$imageName;
        }
    }

    public function userStations()
    {
        return $this->hasMany('App\UserStation');
    }


}

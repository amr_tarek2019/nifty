<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table='sliders';
    protected $fillable=['title_a','title_e','text_a','text_e','image'];

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/slider/'.$value);
        } else {
            return asset('uploads/user/profile/default.png');
        }
    }
    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/slider/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }
}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'authentication','namespace'=>'Api\Authentication'],function () {
    Route::post('register', 'AuthenticationController@register');
    Route::post('login','AuthenticationController@login');
    Route::post('forget/password', 'AuthenticationController@forgetPassword');
    Route::post('verification/code', 'AuthenticationController@verifyCode');
    Route::post('reset/password', 'AuthenticationController@resetPassword');
});

Route::group(['prefix'=>'profile','namespace'=>'Api\Profile'],function () {
    Route::post('user', 'ProfileController@index');
    Route::post('user/update', 'ProfileController@update');
});

Route::group(['prefix'=>'location','namespace'=>'Api\User'],function () {
    Route::post('update', 'UserController@update');
});

Route::group(['prefix'=>'slider','namespace'=>'Api\Slider'],function () {
    Route::get('', 'SliderController@index');
});

Route::group(['prefix'=>'charity','namespace'=>'Api\Charity'],function () {
    Route::get('', 'CharityController@index');
});

Route::group(['prefix'=>'settings','namespace'=>'Api\Settings'],function () {
    Route::get('', 'SettingsController@index');
});

Route::group(['prefix'=>'suggestions','namespace'=>'Api\Suggestion'],function () {
    Route::post('', 'SuggestionController@store');
});

Route::group(['prefix'=>'privacy','namespace'=>'Api\Privacy'],function () {
    Route::get('', 'PrivacyController@index');
});

Route::group(['prefix'=>'stations','namespace'=>'Api\Stations'],function () {
    Route::get('all', 'StationController@index');
    Route::get('rate/all', 'StationController@indexRate');
    Route::post('location/all', 'StationController@indexLocation');
    Route::get('station/show','StationController@show');
    Route::get('station/search/rate','StationController@SearchByRate');
    Route::get('station/search/location','StationController@SearchByLocation');
    //Route::get('station/search/region/E','StationController@SearchByRegionE');
    //Route::get('station/search/region/A','StationController@SearchByRegionA');
});


Route::group(['prefix'=>'qr','namespace'=>'Api\Qr'],function () {
    Route::post('code/station', 'QrController@index');
});

Route::group(['prefix'=>'reviews','namespace'=>'Api\Reviews'],function () {
    Route::post('review/create','ReviewController@create');
    Route::get('review/show','ReviewController@show');
    Route::get('avg/rate/review','ReviewController@getAverageRatingOfStationReviews');
    Route::get('count/review','ReviewController@getCountRatingOfStationReviews');

});

Route::group(['prefix'=>'FuelPaymentDonation','namespace'=>'Api\FuelPaymentDonation'],function () {
    Route::post('create','FuelPaymentDonationController@store');
    Route::get('show','FuelPaymentDonationController@show');
    Route::get('user/history/show','FuelPaymentDonationController@indexHistory');
    Route::get('user/transactions/show','FuelPaymentDonationController@indexTransactions');
});

Route::group(['prefix'=>'wallet','namespace'=>'Api\Wallet'],function () {
    Route::post('user', 'WalletController@index');
    Route::post('user/update', 'WalletController@update');
    Route::post('user/create', 'WalletController@createWallet');
    Route::post('user/otp/code', 'WalletController@verifyOtpCode');
});
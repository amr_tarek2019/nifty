<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix'=>'auth','namespace'=>'Authentication'],function () {
    Route::get('login','AuthenticationController@index')->name('login');
    Route::post('admin/login','AuthenticationController@userLogin')->name('admin.login');
});

Route::group(['prefix'=>'admin','middleware'=>'auth','namespace'=>'Dashboard'],function () {
    Route::get('dashboard','DashboardController@index')->name('dashboard');
    Route::post('logout', 'DashboardController@logout')->name('logout');

     Route::prefix('profile')->group(function (){
         Route::get('','ProfileController@index')->name('profile.index');
         Route::post('/update/{id}','ProfileController@update')->name('profile.update');
     });

    Route::prefix('settings')->group(function (){
        Route::get('','SettingsController@index')->name('settings.index');
        Route::post('/update','SettingsController@update')->name('settings.update');
    });
    Route::prefix('suggestion')->group(function (){
        Route::get('','SuggestionsController@index')->name('suggestion.index');
        Route::get('/show/{id}','SuggestionsController@show')->name('suggestion.show');
        Route::post('/{id}','SuggestionsController@destroy')->name('suggestion.destroy');
    });
    Route::prefix('privacy')->group(function (){
        Route::get('','PrivacyController@index')->name('privacy.index');
        Route::post('/update','PrivacyController@update')->name('privacy.update');
    });
//    Route::resource('slider', 'SlidersController');
    Route::prefix('slider')->group(function (){
        Route::get('','SlidersController@index')->name('slider.index');
        Route::get('/create','SlidersController@create')->name('slider.create');
        Route::post('/store','SlidersController@store')->name('slider.store');
        Route::post('/{id}','SlidersController@destroy')->name('slider.destroy');
        Route::post('/update/{id}','SlidersController@update')->name('slider.update');
        Route::get('/edit/{id}','SlidersController@edit')->name('slider.edit');
    });

    Route::prefix('station')->group(function (){
        Route::get('','StationsController@index')->name('station.index');
        Route::get('/create','StationsController@create')->name('station.create');
        Route::post('/store','StationsController@store')->name('station.store');
        Route::post('/{id}','StationsController@destroy')->name('station.destroy');
        Route::post('/update/{id}','StationsController@update')->name('station.update');
        Route::get('/edit/{id}','StationsController@edit')->name('station.edit');
    });

    Route::prefix('charity')->group(function (){
        Route::get('','CharitiesController@index')->name('charity.index');
        Route::get('/create','CharitiesController@create')->name('charity.create');
        Route::post('/store','CharitiesController@store')->name('charity.store');
        Route::post('/{id}','CharitiesController@destroy')->name('charity.destroy');
        Route::post('/update/{id}','CharitiesController@update')->name('charity.update');
        Route::get('/edit/{id}','CharitiesController@edit')->name('charity.edit');
    });

    Route::prefix('payments')->group(function (){
        Route::get('','PaymentsController@index')->name('payments.index');
        Route::get('/{id}','PaymentsController@show')->name('payments.show');
        Route::post('/{id}','PaymentsController@destroy')->name('payments.destroy');
        Route::get('/invoice/{id}','PaymentsController@generatePdf')->name('payments.invoice.show');
    });

    Route::prefix('user/admin')->group(function (){
        Route::get('','AdminsController@index')->name('admin.index');
        Route::get('/create','AdminsController@create')->name('admin.create');
        Route::post('/store','AdminsController@store')->name('admin.store');
        Route::get('/edit/{id}','AdminsController@edit')->name('admin.edit');
        Route::post('/update/{id}','AdminsController@update')->name('admin.update');
        Route::post('/{id}','AdminsController@destroy')->name('admin.destroy');
    });

    Route::prefix('user/user')->group(function (){
        Route::get('','UsersController@index')->name('user.index');
        Route::get('/create','UsersController@create')->name('user.create');
        Route::post('/store','UsersController@store')->name('user.store');
        Route::get('/edit/{id}','UsersController@edit')->name('user.edit');
        Route::post('/update/{id}','UsersController@update')->name('user.update');
        Route::post('/{id}','UsersController@destroy')->name('user.destroy');
    });

    Route::prefix('user/moderator')->group(function (){
        Route::get('','ModeratorsController@index')->name('moderator.index');
        Route::get('/create','ModeratorsController@create')->name('moderator.create');
        Route::post('/store','ModeratorsController@store')->name('moderator.store');
        Route::get('/edit/{id}','ModeratorsController@edit')->name('moderator.edit');
        Route::post('/update/{id}','ModeratorsController@update')->name('moderator.update');
        Route::post('/{id}','ModeratorsController@destroy')->name('moderator.destroy');
    });

});
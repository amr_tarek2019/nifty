<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/dashboard/images/favicon.ico') }}">
    <!-- App title -->
    <title>Zircos - Responsive Admin Dashboard Template</title>

    <!-- App css -->
    <link href="{{ asset('assets/dashboard/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/dashboard/css/core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/dashboard/css/components.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/dashboard/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/dashboard/css/pages.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/dashboard/css/menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/dashboard/css/responsive.css') }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{ asset('assets/dashboard/js/modernizr.min.js') }}"></script>

</head>


<body class="bg-transparent">

<!-- HOME -->
<section>
    <div class="container-alt">
        <div class="row">
            <div class="col-sm-12">

                <div class="wrapper-page">

                    <div class="m-t-40 account-pages">
                        <div class="text-center account-logo-box">
                            <h2 class="text-uppercase">
                                <a href="index.html" class="text-success">
                                    <span><img src="{{ asset('assets/login/img/grand.png') }}" alt="" height="36"></span>
                                </a>
                            </h2>
                            <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                        </div>
                        <div class="account-content">
                            <form class="form-horizontal" method="POST"  action="{{ route('admin.login') }}">
                                @csrf
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"  id="email" type="email" name="email" value="{{ old('email') }}" required="" placeholder="Username">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" type="password"  name="password" required="" placeholder="Password">
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <div class="checkbox checkbox-success">
                                            <input id="checkbox-signup" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} checked>
                                            <label for="checkbox-signup">
                                                Remember me
                                            </label>
                                        </div>

                                    </div>
                                </div>


                                <div class="form-group account-btn text-center m-t-10">
                                    <div class="col-xs-12">
                                        <button class="btn w-md btn-bordered btn-danger waves-effect waves-light" type="submit">Log In</button>
                                    </div>
                                </div>

                            </form>

                            <div class="clearfix"></div>

                        </div>
                    </div>
                    <!-- end card-box-->




                </div>
                <!-- end wrapper -->

            </div>
        </div>
    </div>
</section>
<!-- END HOME -->

<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{ asset('assets/dashboard/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/detect.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/fastclick.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/waves.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/jquery.scrollTo.min.js') }}"></script>

<!-- App js -->
<script src="{{ asset('assets/dashboard/js/jquery.core.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/jquery.app.js') }}"></script>

</body>
</html>
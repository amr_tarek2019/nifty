@extends('dashboard.layouts.master')
@section('content')
    <div class="content">
        <div class="container">


            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Create Admin </h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{route('dashboard')}}">Nift</a>
                            </li>
                            <li>
                                <a href="{{route('admin.index')}}">Admins </a>
                            </li>
                            <li class="active">
                                Create Admin
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-lg-12 col-md-4">
                                @include('dashboard.partials.msg')

                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Create Admin </h4>

                                    <form method="POST" action="{{ route('admin.store') }}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="username">Name</label>
                                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="useremail">Email address</label>
                                            <input type="email" class="form-control" name="email" id="email"  placeholder="Enter email">
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label class="form-label" for="exampleFormControlSelect9">User Status select</label>
                                                <select class="form-control digits" name="user_status" id="user_status">
                                                    <option value="0">Deactive</option>
                                                    <option value="1">Active</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label class="form-label" for="exampleFormControlSelect9">Status select</label>
                                                <select class="form-control digits" name="status" id="status">
                                                    <option value="0">Deactive</option>
                                                    <option value="1">Active</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="message">password</label>
                                            <input class="form-control" type="password" name="password" id="password">
                                        </div>
                                        <button type="submit" class="btn btn-purple waves-effect waves-light">Submit</button>
                                    </form>
                                </div> <!-- end card-box -->

                            </div> <!-- end col -->



                        </div>
                    </div>
                </div>
            </div>
            <!-- End row -->



        </div> <!-- container -->

    </div> <!-- content -->

@endsection
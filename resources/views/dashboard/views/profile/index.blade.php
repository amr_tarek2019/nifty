@extends('dashboard.layouts.master')
@section('content')
    <div class="content">
        <div class="container">


            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Admin Profile </h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{route('dashboard')}}">Nift</a>
                            </li>
                            <li>
                                <a href="{{route('profile.index')}}">Profile </a>
                            </li>
                            <li class="active">
                                Edit Admin Profile
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-lg-12 col-md-4">
                                @include('dashboard.partials.msg')

                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Edit Admin Profile</h4>

                                    <form method="POST" action="{{ route('profile.update',Auth::user()->id) }}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="username">Name</label>
                                            <input type="text" class="form-control" name="name" id="name" value="{{$user->name}}" placeholder="Enter Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="useremail">Email address</label>
                                            <input type="email" class="form-control" name="email" id="email" value="{{$user->email}}" placeholder="Enter email">
                                        </div>
                                        <div class="form-group">
                                            <img src="{{ asset($user->image) }}" alt="image" class="img-responsive img-thumbnail" width="100">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Default file input</label>
                                            <input type="file" class="filestyle" data-buttonname="btn-default" id="image" name="image" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">
                                        </div>

                                        <div class="form-group">
                                            <label for="message">password</label>
                                            <input class="form-control" type="password" name="password" id="password">
                                        </div>
                                        <button type="submit" class="btn btn-purple waves-effect waves-light">Submit</button>
                                    </form>
                                </div> <!-- end card-box -->

                            </div> <!-- end col -->



                        </div>
                    </div>
                </div>
            </div>
            <!-- End row -->



        </div> <!-- container -->

    </div> <!-- content -->

@endsection
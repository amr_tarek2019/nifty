@extends('dashboard.layouts.master')
@section('content')
    <div class="content">
        <div class="container">


            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Create Slider </h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Nift</a>
                            </li>
                            <li>
                                <a href="#">Slider </a>
                            </li>
                            <li class="active">
                               Create Slider
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div class="row">
                <div class="col-xs-12">
                    <div class="card-box">

                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-6">


                                <div class="p-20">
                                    <form method="POST" action="{{ route('slider.store') }}" enctype="multipart/form-data" data-parsley-validate novalidate>
                                        @csrf
                                        <div class="form-group">
                                            <label for="userName">Arabic Title</label>
                                            <input type="text" name="title_a" parsley-trigger="change" required
                                                   placeholder="Enter Arabic Title" class="form-control" id="title_a">
                                        </div>
                                        <div class="form-group">
                                            <label for="userName">English Title</label>
                                            <input type="text" name="title_e" parsley-trigger="change" required
                                                   placeholder="Enter English Title" class="form-control" id="title_e">
                                        </div>
                                        <div class="form-group">
                                            <label>Arabic Text</label>
                                            <div>
                                                <textarea type="text" name="text_a" id="text_a" required class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>English Text</label>
                                            <div>
                                                <textarea name="text_e" id="text_e" required class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Default file input</label>
                                            <input type="file" class="filestyle" data-buttonname="btn-default" id="image" name="image" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">
                                        </div>
                                        <div class="form-group text-right m-b-0">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-default waves-effect m-l-5">
                                                Cancel
                                            </button>
                                        </div>

                                    </form>
                                </div>

                            </div>


                        </div>


                    </div> <!-- end ard-box -->
                </div><!-- end col-->

            </div>
            <!-- end row -->


        </div> <!-- container -->



</div>


@endsection
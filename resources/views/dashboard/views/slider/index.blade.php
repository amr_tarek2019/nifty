@extends('dashboard.layouts.master')
@section('content')
    <div class="content">
        <div class="container">


            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Sliders </h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Nift</a>
                            </li>
                            <li class="active">
                                Sliders
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <a href="{{ route('slider.create') }}" class="btn btn-primary">Add New</a>
            @include('dashboard.partials.msg')
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <h4 class="m-t-0 header-title"><b>Sliders Table</b></h4>


                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>


                                <tr>
                                <th>#</th>
                                <th>Arabic Title</th>
                                <th>English Title</th>
                                <th>Image</th>
                                <th>Actions</th>
                            </tr>
                            </thead>


                            <tbody>
                            @foreach($sliders as $key=>$slider)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $slider->title_a }}</td>
                                <td>{{ $slider->title_e }}</td>
                                <td><img class="img-responsive img-thumbnail" src="{{ asset($slider->image) }}" style="height: 100px; width: 100px" alt=""></td>
                                <td>
                                    <a href="{{ route('slider.edit',$slider->id) }}" class="btn btn-info btn-sm"><i class="material-icons"><i class="mdi mdi-pencil-box"></i></i></a>

                                    <form id="delete-form-{{ $slider->id }}" action="{{ route('slider.destroy',$slider->id) }}" style="display: none;" method="POST">
                                        @csrf
                                    </form>
                                    <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure? You want to delete this?')){
                                            event.preventDefault();
                                            document.getElementById('delete-form-{{ $slider->id }}').submit();
                                            }else {
                                            event.preventDefault();
                                            }"><i class="material-icons"><i class="mdi mdi-delete"></i></i></button>
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </div> <!-- container -->
    </div>

@endsection



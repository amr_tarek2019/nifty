@extends('dashboard.layouts.master')
@section('content')
    <div class="content">
        <div class="container">


            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Settings</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Nift</a>
                            </li>
                            <li class="active">
                                Settings
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div class="row">
                <div class="col-xs-12">
                    <div class="card-box">
                        @include('dashboard.partials.msg')
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-6">



                                <div class="p-20">
                                    <form action="{{route('settings.update')}}" method="POST"  >
                                          @csrf
                                         <div class="form-group">
                                            <label for="userName">phone</label>
                                            <input type="text" name="phone" parsley-trigger="change"
                                                   placeholder="Enter phone" class="form-control" id="phone"
                                            value="{{$settings->phone}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="emailAddress">Email address</label>
                                            <input type="email" name="email" parsley-trigger="change"
                                                   placeholder="Enter email" class="form-control" id="email"
                                                   value="{{$settings->email}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="website">website</label>
                                            <input id="website" type="text" name="website" placeholder="website"
                                                   class="form-control"  value="{{$settings->website}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="facebook">facebook</label>
                                            <input type="text" name="facebook" parsley-trigger="change"
                                                   placeholder="Enter facebook" class="form-control" id="facebook"
                                                   value="{{$settings->facebook}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="whatsapp">whatsapp</label>
                                            <input type="text" name="whatsapp" parsley-trigger="change"
                                                   placeholder="Enter whatsapp" class="form-control" id="whatsapp"
                                                   value="{{$settings->whatsapp}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="instagram">instagram</label>
                                            <input type="text" name="instagram" parsley-trigger="change"
                                                   placeholder="Enter instagram" class="form-control" id="instagram"
                                                   value="{{$settings->instagram}}">
                                        </div>
                                        <div class="form-group text-right m-b-0">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                Submit
                                            </button>
                                            <a href="{{route('dashboard')}}" class="btn btn-default waves-effect m-l-5">
                                                Cancel
                                            </a>
                                        </div>

                                    </form>
                                </div>

                            </div>


                        </div>
                        <!-- end row -->



                    </div> <!-- end ard-box -->
                </div><!-- end col-->

            </div>
            <!-- end row -->


        </div> <!-- container -->


</div>



@endsection
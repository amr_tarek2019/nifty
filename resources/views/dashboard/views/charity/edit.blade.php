@extends('dashboard.layouts.master')
@section('content')
    <div class="content">
        <div class="container">


            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Update Charity </h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Nift</a>
                            </li>
                            <li>
                                <a href="#">Charities </a>
                            </li>
                            <li class="active">
                                Update Charity
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div class="row">
                <div class="col-xs-12">
                    <div class="card-box">

                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-6">


                                <div class="p-20">
                                    <form method="POST" action="{{ route('charity.update',$charity->id) }}" enctype="multipart/form-data" data-parsley-validate novalidate>
                                        @csrf
                                        <div class="form-group">
                                            <label for="userName">Arabic Name</label>
                                            <input type="text" name="name_a" parsley-trigger="change" required
                                              value="{{$charity->name_a}}"  placeholder="Enter Arabic Name" class="form-control" id="name_a">
                                        </div>
                                        <div class="form-group">
                                            <label for="userName">English Name</label>
                                            <input type="text" name="name_e" parsley-trigger="change" required
                                                   value="{{$charity->name_e}}"      placeholder="Enter English Name" class="form-control" id="name_e">
                                        </div>
                                        <div class="form-group">
                                            <img src="{{ asset($charity->image) }}" alt="image" class="img-responsive img-thumbnail" width="100">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Default file input</label>
                                            <input type="file" class="filestyle" data-buttonname="btn-default" id="image" name="image" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">
                                        </div>
                                        <div class="form-group text-right m-b-0">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-default waves-effect m-l-5">
                                                Cancel
                                            </button>
                                        </div>

                                    </form>
                                </div>

                            </div>


                        </div>


                    </div> <!-- end ard-box -->
                </div><!-- end col-->

            </div>
            <!-- end row -->


        </div> <!-- container -->



    </div>


@endsection
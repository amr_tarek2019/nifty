@extends('dashboard.layouts.master')
@section('content')
<div class="content">
    <div class="container">


        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Payment Information </h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="#">Nift</a>
                        </li>
                        <li>
                            <a href="#">Payments </a>
                        </li>
                        <li class="active">
                            Payment Information
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-xs-12">
                <div class="card-box">

                    <div class="row">
                        <div class="col-sm-12 col-xs-12 col-md-6">

                            <h4 class="header-title m-t-0">User Information</h4>
                            <div class="p-20">
                                    <div class="form-group">
                                        <label for="userName">User Name</label>
                                        <input type="text" name="name" value="{{$payment->user->name}}" readonly class="form-control" id="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="emailAddress">Email address</label>
                                        <input type="email" name="email" value="{{$payment->user->email}}" readonly class="form-control" id="email" readonly>
                                    </div>
                                <div class="form-group">
                                    <label>Phone</label>
                                    <div>
                                        <input type="tel" class="form-control" readonly name="phone" id="phone" value="{{$payment->user->phone}}">
                                    </div>
                                </div>

                            </div>

                        </div>

                            <div class=" col-sm-12 col-xs-12 col-md-6">
                                <h4 class="header-title m-t-0">Payment Information</h4>


                                <div class="p-20">
                                    <div class="form-group">
                                        <label>date</label>
                                        <input type="date" class="form-control" readonly
                                               name="date" value="{{$payment->date}}"/>
                                    </div>


                                    <div class="form-group">
                                        <label>Fuel Amount Paid</label>
                                        <div>
                                            <input type="number" id="Fuel_Amount_Paid" class="form-control" readonly
                                                   name="Fuel_Amount_Paid" value="{{$payment->Fuel_Amount_Paid}}"/>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label>Donation Amount</label>
                                        <div>
                                            <input type="number" class="form-control" id="Donation_Amount"
                                                   readonly name="Donation_Amount" value="{{$payment->Donation_Amount}}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>order number</label>
                                        <div>
                                            <input type="number" class="form-control" id="order_number"
                                                   readonly name="order_number" value="{{$payment->order_number}}"/>
                                        </div>
                                    </div>


                                </div>

                            </div>





                        <div class="col-sm-12 col-xs-12 col-md-6">
                            <h4 class="header-title m-t-0">Station Information</h4>

                            <div class="p-20">
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 form-control-label">English Name</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="name_e" class="form-control" readonly id="name_e" value="{{$payment->station->name_e}}">
                                        </div>
                                    </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 form-control-label">Arabic Name</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="name_a" class="form-control" readonly id="name_a" value="{{$payment->station->name_a}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 form-control-label">Region in arabic</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="region_a" class="form-control" readonly id="region_a" value="{{$payment->station->region_a}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 form-control-label">Region in english</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="region_e" class="form-control" readonly id="region_e" value="{{$payment->station->region_e}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Description in english</label>
                                    <div>
                                        <textarea name="description_e" class="form-control" readonly id="description_e" class="form-control">{{$payment->station->description_e}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Description in arabic</label>
                                    <div>
                                        <textarea name="description_a" class="form-control" readonly id="description_a" class="form-control">{{$payment->station->description_a}}</textarea>
                                    </div>
                                </div>
                                    <div class="form-group row">
                                        <label for="hori-pass1" class="col-sm-4 form-control-label">Rate</label>
                                        <div class="col-sm-7">
                                            <input id="rate" type="number" readonly
                                                   class="form-control" value="{{$payment->station->rate}}">
                                        </div>
                                    </div>
                                <div class="form-group row">
                                    <label for="hori-pass1" class="col-sm-4 form-control-label">Qr Code</label>
                                    <div class="col-sm-7">
                                        <input id="qr_code" type="number" readonly
                                               class="form-control" value="{{$payment->station->qr_code}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="hori-pass1" class="col-sm-4 form-control-label">Main image</label>
                                    <img src="{{ asset($payment->station->image) }}" alt="image" class="img-responsive img-thumbnail" width="100">
                                </div>
                                <div class="form-group">
                                    <label for="hori-pass1" class="col-sm-4 form-control-label">Qr image</label>
                                    <img src="{{ asset($payment->station->qr) }}" alt="image" class="img-responsive img-thumbnail" width="100">
                                </div>
                                <div class="form-group">
                                    <label for="hori-pass1" class="col-sm-4 form-control-label">icon</label>
                                    <img src="{{ asset($payment->station->icon) }}" alt="image" class="img-responsive img-thumbnail" width="100">
                                </div>
                                <div class="form-group">
                                    <label for="hori-pass1" class="col-sm-4 form-control-label">station slider</label>
                                    <img src="{{ asset($payment->station->image1) }}" alt="image" class="img-responsive img-thumbnail" width="100">
                                    <img src="{{ asset($payment->station->image2) }}" alt="image" class="img-responsive img-thumbnail" width="100">

                                </div>



                            </div>
                            <div class="form-group m-b-0">
                                <div>
                                    <a href="{{route('payments.index')}}" type="submit" class="btn btn-primary waves-effect waves-light">
                                        back
                                    </a>

                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- end row -->


                    <!-- end row -->

                </div> <!-- end ard-box -->
            </div><!-- end col-->

        </div>
        <!-- end row -->


    </div> <!-- container -->
</div>
@endsection
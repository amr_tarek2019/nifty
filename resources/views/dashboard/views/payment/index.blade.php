@extends('dashboard.layouts.master')
@section('content')
    <div class="content">
        <div class="container">


            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Payments </h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Nift</a>
                            </li>
                            <li class="active">
                                Payments
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            @include('dashboard.partials.msg')
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <h4 class="m-t-0 header-title"><b>Payments Table</b></h4>
                        <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 163px;">
                                        #</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 163px;">
                                        User Name</th>
                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 274px;">
                                        Station Name</th>
                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 116px;">
                                        Date</th>
                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 51px;">
                                        Fuel Amount Paid</th>
                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 104px;">
                                        Donation Amount</th>
                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 97px;">
                                        Order Number</th>
                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 97px;">
                                        Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payments as $key=>$payment)
                                <tr role="row" class="odd">
                                    <td class="sorting_1" tabindex="0">{{ $key + 1 }}</td>
                                    <td>{{ $payment->user->name }}</td>
                                    <td>{{ $payment->station->name_a }}</td>
                                    <td>{{ $payment->date}}</td>
                                    <td>{{ $payment->Fuel_Amount_Paid}}</td>
                                    <td>{{ $payment->Donation_Amount}}</td>
                                    <td>{{ $payment->order_number}}</td>
                                    <td style="
    width: 115px;"   >    <a href="{{ route('payments.show',$payment->id) }}" class="btn btn-info btn-sm"><i class="material-icons"><i class="mdi mdi-eye"></i></i></a>
                                        <form id="delete-form-{{ $payment->id }}" action="{{ route('payments.destroy',$payment->id) }}" style="display: none;" method="POST">
                                            @csrf
                                        </form>
                                        <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure? You want to delete this?')){
                                                event.preventDefault();
                                                document.getElementById('delete-form-{{ $payment->id }}').submit();
                                                }else {
                                                event.preventDefault();
                                                }"><i class="material-icons"><i class="mdi mdi-delete"></i></i></button>
                                                 <a href="{{route('payments.invoice.show',$payment->id)}}" class="btn btn-warning btn-sm"> <i class="mdi mdi-printer"></i> </a>

                                    </td>
                                </tr>
                                    @endforeach
                               </tbody>
                            </table>


                    </div>
                </div>
            </div>



        </div> <!-- container -->
    </div>
    </div>
@endsection



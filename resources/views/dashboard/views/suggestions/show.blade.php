@extends('dashboard.layouts.master')
@section('content')
<div class="content">
    <div class="container">


        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Suggestion Read </h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{route('dashboard')}}">Nift</a>
                        </li>
                        <li>
                            <a href="{{route('suggestion.index')}}">Suggestion </a>
                        </li>
                        <li class="active">
                            Suggestion Read
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-xs-12">
                <div class="mails">

                    <div class="table-box">

                        <div class="table-detail mail-right">



                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box m-t-20">


                                        <div class="media m-b-30 ">
                                            <a href="#" class="pull-left">
                                                <img alt="" src="{{$suggestion->user->image}}" class="media-object thumb-sm img-circle">
                                            </a>
                                            <div class="media-body">
                                                <span class="media-meta pull-right">{{$suggestion->created_at}}</span>
                                                <h4 class="text-primary m-0">{{$suggestion->user->name}}</h4>
                                                <small class="text-muted">From: {{$suggestion->email}}</small>
                                            </div>
                                        </div> <!-- media -->


                                        <p>{{$suggestion->suggestion}}</p>

                                        <hr/>


                                    </div>
                                    <!-- card-box -->
                                </div> <!-- end col -->
                            </div> <!-- end row -->

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="text-right">
                                        <a href="{{route('suggestion.index')}}" type="button" class="btn btn-primary waves-effect waves-light w-md m-b-30"><i class="mdi mdi-reply m-r-10"></i>Back</a>
                                    </div>
                                </div>
                            </div>

                            <!-- End row -->

                        </div> <!-- table detail -->
                    </div>
                    <!-- end table box -->

                </div> <!-- mails -->
            </div>
        </div>
        <!-- end row -->

    </div> <!-- container -->
</div>
@endsection
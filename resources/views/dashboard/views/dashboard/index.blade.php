@extends('dashboard.layouts.master')
@section('content')

    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Dashboard</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li class="active">
                                Nift
                            </li>
                            <li>
                                <a href="{{route('dashboard')}}">Dashboard</a>
                            </li>

                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row">

                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-chart-areaspline widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Stations</p>
                            <h2>{{ \App\Station::all()->count() }} <small><i class="mdi mdi-arrow-up text-success"></i></small></h2>
                        </div>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-account-convert widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User Today">Moderators</p>
                            <h2>{{ \App\User::where('user_type', 'moderator')->count() }} <small><i class="mdi mdi-arrow-down text-danger"></i></small></h2>
                        </div>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-layers widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">Charties</p>
                            <h2>{{ \App\Charity::all()->count() }} <small><i class="mdi mdi-arrow-up text-success"></i></small></h2>
                        </div>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-av-timer widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Request Per Minute">Requests</p>
                            <h2>{{ \App\UserStation::all()->count() }} <small><i class="mdi mdi-arrow-down text-danger"></i></small></h2>
                        </div>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-account-multiple widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Total Users">Total Users</p>
                            <h2>{{ \App\User::where('user_type', 'user')->count() }} <small><i class="mdi mdi-arrow-down text-danger"></i></small></h2>
                        </div>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-download widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="New Downloads">Suggestions</p>
                            <h2>{{ \App\Suggestion::all()->count() }} <small><i class="mdi mdi-arrow-up text-success"></i></small></h2>
                        </div>
                    </div>
                </div><!-- end col -->

            </div>
            <!-- end row -->


            <div class="row">


                <div class="col-lg-4">
                    <div class="card-box">

                        <h4 class="header-title m-t-0">Users</h4>
                        <div id="myfirstchart" style="height: 250px;"></div>

                    </div>
                </div><!-- end col -->


                <div class="col-lg-4">
                    <div class="card-box">

                        <h4 class="header-title m-t-0">Stations</h4>
                        <div id="mySecondChart" style="height: 250px;"></div>

                    </div>
                </div><!-- end col -->

                <div class="col-lg-4">
                    <div class="card-box">

                        <h4 class="header-title m-t-0">Charities</h4>
                        <div id="myThirdChart" style="height: 250px;"></div>

                    </div>
                </div><!-- end col -->
            </div>
                <!-- end row -->
<br>

            <div class="row">
                <div class="col-lg-6">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30">Recent Users</h4>

                        <div class="table-responsive">
                            <table class="table table table-hover m-0">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>User Name</th>
                                    <th>Phone</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                             @php
                                 $users = \App\User::where('user_type','user')->take('4')->orderBy('id','desc')->get();;
                             @endphp
                             @foreach($users as $user)
                                <tr>
                                    <th>
                                        <img src="{{ asset($user->image) }}" alt="user" class="thumb-sm img-circle" />
                                    </th>
                                    <td>
                                        <h5 class="m-0">{{$user->name}}</h5>
                                        <p class="m-0 text-muted font-13"><small>{{$user->email}}</small></p>
                                    </td>
                                    <td>{{$user->phone}}</td>
                                    <td>{{$user->created_at}}</td>
                                </tr>
                              @endforeach


                                </tbody>
                            </table>

                        </div> <!-- table-responsive -->
                    </div> <!-- end card -->
                </div>
                <!-- end col -->

                <div class="col-lg-6">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30">Recent Stations</h4>

                        <div class="table-responsive">
                            <table class="table table table-hover m-0">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Rate</th>
                                    <th>Qr Code</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $stations = \App\Station::take('4')->orderBy('id','desc')->get();
                                @endphp
                                @foreach($stations as $station)
                                <tr>
                                    <th>
                                        <img src="{{ asset($station->icon) }}" alt="user" class="thumb-sm img-circle" />
                                    </th>
                                    <td>
                                        <h5 class="m-0">{{$station->name_e}}</h5>
                                        <p class="m-0 text-muted font-13"><small>{{$station->region_e}}</small></p>
                                    </td>
                                    <td>{{$station->rate}}</td>
                                    <td>{{$station->qr_code}}</td>
                                    <td>{{$station->created_at}}</td>
                                </tr>
                                @endforeach


                                </tbody>
                            </table>

                        </div> <!-- table-responsive -->
                    </div> <!-- end card -->
                </div>
                <!-- end col -->

            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-lg-6">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30">Recent Charities</h4>

                        <div class="table-responsive">
                            <table class="table table table-hover m-0">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $charities = \App\Charity::take('4')->orderBy('id','desc')->get();
                                @endphp
                                @foreach($charities as $charity)
                                    <tr>
                                        <th>
                                            <img src="{{ asset($charity->image) }}" alt="user" class="thumb-sm img-circle" />
                                        </th>
                                        <td>
                                            <h5 class="m-0">{{$charity->name_e}}</h5>
                                        </td>
                                        <td>{{$charity->created_at}}</td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>

                        </div> <!-- table-responsive -->
                    </div> <!-- end card -->
                </div>
                <!-- end col -->

                <div class="col-lg-6">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30">Recent Requests</h4>

                        <div class="table-responsive">
                            <table class="table table table-hover m-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Station Name</th>
                                    <th>User Name</th>
                                    <th>Fuel Amount Paid</th>
                                    <th>Donation Amount</th>
                                    <th>Order Number</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $payments = \App\UserStation::take('4')->orderBy('id','desc')->get();
                                @endphp
                                @foreach($payments as $key=>$payment)
                                    <tr>

                                        <td>{{$key+1}}</td>
                                        <td>
                                            <h5 class="m-0">{{ $payment->station->name_a }}</h5>
                                        </td>
                                        <td>{{ $payment->user->name }}</td>
                                        <td>{{$payment->Fuel_Amount_Paid}}</td>
                                        <td>{{$payment->Donation_Amount}}</td>
                                        <td>{{$payment->order_number}}</td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>

                        </div> <!-- table-responsive -->
                    </div> <!-- end card -->
                </div>
                <!-- end col -->

            </div>


        </div> <!-- container -->

  <!-- content -->



</div>
@endsection
@section('myjsfile')
    <script>
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'myfirstchart',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                    @foreach($numbers_of_users as $user)

                { day: '{{$user->date}}', value: '{{$user->count}}' },
                @endforeach
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'day',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Value']
        });
    </script>
    <script>
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'mySecondChart',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                    @foreach($numbers_of_stations as $station)

                { day: '{{$station->date}}', value: '{{$station->count}}' },
                @endforeach
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'day',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Value']
        });
    </script>
    <script>
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'myThirdChart',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                    @foreach($numbers_of_charities as $charity)

                { day: '{{$charity->date}}', value: '{{$charity->count}}' },
                @endforeach
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'day',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Value']
        });
    </script>
@stop
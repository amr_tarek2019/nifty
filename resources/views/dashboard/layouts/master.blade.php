<!DOCTYPE html>
@if(request()->segment(1)=='en')

    <html lang="en" dir="ltr">
    @else
        <html lang="ar" dir="rtl">
        @endif
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/dashboard/images/fav_icon.png') }}">
    <!-- App title -->
    <title>Nifty - Admin Dashboard</title>

    <!-- Plugins css-->
    <link href="{{ asset('assets/dashboard/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/dashboard/plugins/multiselect/css/multi-select.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/dashboard/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/dashboard/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/dashboard/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{ asset('assets/dashboard/plugins/morris/morris.css') }}">

    <!-- DataTables -->
    <link href="{{ asset('assets/dashboard/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/dashboard/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/dashboard/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/dashboard/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/dashboard/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/dashboard/plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/dashboard/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/dashboard/plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- App css -->
    <link href="{{ asset('assets/dashboard/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/dashboard/css/core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/dashboard/css/components.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/dashboard/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/dashboard/css/pages.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/dashboard/css/menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/dashboard/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/dashboard/plugins/switchery/switchery.min.css') }}">

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet"/>1 <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">


    <![endif]-->

    <script src="{{ asset('assets/dashboard/js/modernizr.min.js') }}"></script>

</head>

        @if(request()->segment(1)=='ar')
            <body main-theme-layout="rtl">

            @else
<body class="fixed-left">
@endif

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
@include('dashboard.partials.navbar')

<!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->
@include('dashboard.partials.sidebar')
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
@yield('content')
    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
    @include('dashboard.partials.footer')



</div>
<!-- END wrapper -->



<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{ asset('assets/dashboard/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/detect.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/fastclick.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/waves.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/jquery.scrollTo.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/switchery/switchery.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/jquery-quicksearch/jquery.quicksearch.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
<!-- Counter js  -->
<script src="{{ asset('assets/dashboard/plugins/waypoints/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/counterup/jquery.counterup.min.js') }}"></script>

<!--Morris Chart-->
<script src="{{ asset('assets/dashboard/plugins/morris/morris.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/raphael/raphael-min.js') }}"></script>

<!-- Dashboard init -->
<script src="{{ asset('assets/dashboard/pages/jquery.dashboard.js') }}"></script>

<!-- App js -->
<script src="{{ asset('assets/dashboard/js/jquery.core.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/jquery.app.js') }}"></script>

<script src="{{ asset('assets/dashboard/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/datatables/dataTables.bootstrap.js') }}"></script>

<script src="{{ asset('assets/dashboard/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/datatables/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/datatables/dataTables.colVis.js') }}"></script>
<script src="{{ asset('assets/dashboard/plugins/datatables/dataTables.fixedColumns.min.js') }}"></script>

<!-- init -->
<script src="{{ asset('assets/dashboard/pages/jquery.datatables.init.js') }}"></script>

<!-- App js -->
<script src="{{ asset('assets/dashboard/assets/js/jquery.core.js') }}"></script>
<script src="{{ asset('assets/dashboard/assets/js/jquery.app.js') }}"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<script>
    $(document).ready(function () {
        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({keys: true});
        $('#datatable-responsive').DataTable();
        $('#datatable-colvid').DataTable({
            "dom": 'C<"clear">lfrtip',
            "colVis": {
                "buttonText": "Change columns"
            }
        });
        $('#datatable-scroller').DataTable({
            ajax: "{{ asset('assets/dashboard/plugins/datatables/json/scroller-demo.json') }}",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });
        var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
        var table = $('#datatable-fixed-col').DataTable({
            scrollY: "300px",
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            fixedColumns: {
                leftColumns: 1,
                rightColumns: 1
            }
        });
    });
    TableManageButtons.init();

</script>
<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@yield('myjsfile')
</body>
</html>
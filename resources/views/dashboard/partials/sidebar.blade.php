<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul>
                <li class="menu-title">Navigation</li>
                @if(Auth::user()->user_type=='admin')
                <li>
                    <a href="{{route('dashboard')}}" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span> </a>
                </li>
                @endif
                @if(Auth::user()->user_type=='admin')
                <li>
                    <a href="{{route('admin.index')}}" class="waves-effect"><i class="mdi mdi-account-multiple"></i> <span> Admins </span> </a>
                </li>
                @endif
                @if(Auth::user()->user_type=='admin'||Auth::user()->user_type=='moderator')
                <li>
                    <a href="{{route('moderator.index')}}" class="waves-effect"><i class="mdi mdi-account-multiple-plus"></i> <span> Moderators </span> </a>
                </li>
                @endif
                @if(Auth::user()->user_type=='admin')
                <li>
                    <a href="{{route('user.index')}}" class="waves-effect"><i class="mdi mdi-account-plus"></i> <span> Users </span> </a>
                </li>
                @endif
                @if(Auth::user()->user_type=='admin'||Auth::user()->user_type=='moderator')

                <li>
                    <a href="{{route('station.index')}}" class="waves-effect"><i class="mdi mdi-gas-station"></i> <span> Stations </span> </a>
                </li>
                @endif
                @if(Auth::user()->user_type=='admin'||Auth::user()->user_type=='moderator')

                <li>
                    <a href="{{route('charity.index')}}" class="waves-effect"><i class="mdi mdi-home-map-marker"></i> <span> Charities </span> </a>
                </li>
                @endif
                @if(Auth::user()->user_type=='admin'||Auth::user()->user_type=='moderator')

                <li>
                    <a href="{{route('payments.index')}}" class="waves-effect"><i class="mdi mdi-cash-multiple"></i> <span> Payments </span> </a>
                </li>
                @endif
                @if(Auth::user()->user_type=='admin')
                <li>
                    <a href="{{route('slider.index')}}" class="waves-effect"><i class="mdi mdi-image-filter"></i> <span> Sliders </span> </a>
                </li>
                @endif
                @if(Auth::user()->user_type=='admin')
                <li>
                    <a href="{{route('privacy.index')}}" class="waves-effect"><i class="mdi mdi-paperclip"></i> <span> Privacy </span> </a>
                </li>
                @endif
                @if(Auth::user()->user_type=='admin')
                <li>
                    <a href="{{route('suggestion.index')}}" class="waves-effect"><i class="mdi mdi-email"></i> <span> Suggestions </span> </a>
                </li>
                @endif
                @if(Auth::user()->user_type=='admin')
                <li>
                    <a href="#" class="waves-effect"><i class="mdi mdi-bell"></i> <span> Push Notification </span> </a>
                </li>
                @endif
                @if(Auth::user()->user_type=='admin')
                <li>
                    <a href="{{route('settings.index')}}" class="waves-effect"><i class="mdi mdi-wrench"></i> <span> Settings </span> </a>
                </li>
                @endif
            </ul>
        </div>


    </div>
    <!-- Sidebar -left -->

</div>
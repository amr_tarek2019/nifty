<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left">
        <a href="index.html" class="logo"><span>Ni<span>ft</span></span><i class="mdi mdi-layers"></i></a>
        <!-- Image logo -->
        <!--<a href="index.html" class="logo">-->
        <!--<span>-->
        <!--<img src="{{ asset('assets/dashboard/images/logo.png') }}" alt="" height="30">-->
        <!--</span>-->
        <!--<i>-->
        <!--<img src="{{ asset('assets/dashboard/images/logo_sm.png') }}" alt="" height="28">-->
        <!--</i>-->
        <!--</a>-->
    </div>

    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">

            <!-- Navbar-left -->
            <ul class="nav navbar-nav navbar-left">
                <li>
                    <button class="button-menu-mobile open-left waves-effect">
                        <i class="mdi mdi-menu"></i>
                    </button>
                </li>


                <li class="dropdown hidden-xs">
                    <a data-toggle="dropdown" class="dropdown-toggle menu-item" href="#" aria-expanded="false">English
                        <span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)

                        <li><a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}"> {{ $properties['native'] }}</a></li>
                        @endforeach

                    </ul>
                </li>
            </ul>

            <!-- Right(Notification) -->
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="#" class="right-menu-item dropdown-toggle" data-toggle="dropdown">
                        <i class="mdi mdi-bell"></i>
                        <span class="badge up bg-success">@php
                                $requests = DB::table('users')
                                            ->orderBy('id', 'desc')
                                            ->where('user_status', '0')
                                            ->where('user_type', 'user')
                                            ->select('id')
                                            ->distinct()
                                            ->count();
                                echo $requests;
                            @endphp</span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right dropdown-lg user-list notify-list">
                        <li>
                            <h5>Users Requests</h5>
                        </li>
                        @if(countUnActiveUser()>0)
                            @foreach(unActiveUser() as $keyMessages => $unActiveUser)
                        <li>
                            <a href="{{route('user.edit',$unActiveUser->id)}}" class="user-list-item">
                                <div class="icon bg-info">
                                    <i class="mdi mdi-account"></i>
                                </div>
                                <div class="user-desc">
                                    <span class="name">{{$unActiveUser->name}}</span>
                                    <span class="time">{{$unActiveUser->created_at}}</span>
                                </div>
                            </a>
                        </li>
                            @endforeach
                        <li class="all-msgs text-center">
                            <p class="m-0"><a href="{{route('user.index')}}">See all Requests</a></p>
                        </li>
                                @else
                                                            <li class="all-msgs text-center">
                                                                <p class="m-0"><a href="#">No Requests Found</a></p>
                                                            </li>
                                                        @endif
                    </ul>
                </li>

                <li>
                    <a href="#" class="right-menu-item dropdown-toggle" data-toggle="dropdown">
                        <i class="mdi mdi-email"></i>
                        <span class="badge up bg-danger">   @php
                                $suggestions = DB::table('suggestions')
                                            ->orderBy('id', 'desc')
                                            ->where('view', 0)
                                            ->select('id')
                                            ->distinct()
                                            ->count();
                                echo $suggestions;
                            @endphp
                        </span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right dropdown-lg user-list notify-list">
                        <li>
                            <h5>Messages</h5>
                        </li>
                        @if(countUnreadMsg()>0)
                        @foreach(unreadMsg() as $keyMessages => $valueMessage)
                        <li>
                            <a href="{{route('suggestion.show',$valueMessage->id)}}" class="user-list-item">
                                <div class="avatar">
                                    <img src="{{$valueMessage->user->image}}" alt="">
                                </div>
                                <div class="user-desc">
                                    <span class="name">{{$valueMessage->user->name}}</span>
                                    <span class="desc">{{str_limit($valueMessage->message,10)}}</span>
                                    <span class="time">{{$valueMessage->created_at}}</span>
                                </div>
                            </a>
                        </li>
                        @endforeach
                            <li class="all-msgs text-center">
                                <p class="m-0"><a href="{{route('suggestion.index')}}">See all Messages</a></p>
                            </li>
                        @else
                        <li class="all-msgs text-center">
                            <p class="m-0"><a href="#">No Messages Found</a></p>
                        </li>
                    @endif

                    </ul>
                </li>



                <li class="dropdown user-box">
                    <a href="" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                        <img src="{{Auth::User()->image}}" alt="user-img" class="img-circle user-img">
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                        <li>
                            <h5>Hi, {{Auth::User()->name}}</h5>
                        </li>
                        <li><a href="{{route('profile.index')}}"><i class="ti-user m-r-5"></i> Profile</a></li>
                        <li><a href="{{route('settings.index')}}"><i class="ti-settings m-r-5"></i> Settings</a></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                                <i class="ti-power-off m-r-5"></i> Logout
                            </a></li>
                        <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </ul>
                </li>

            </ul> <!-- end navbar-right -->

        </div><!-- end container -->
    </div><!-- end navbar -->
</div>